# Free Rings

A FOSS recreation of Ninjakiwi's Rings game in HTML5.

Livestream series where I developed it:
https://www.youtube.com/playlist?list=PLALx9d_ycE5uirkxkRsLFUqIMdwJptzIB

![Screenshot](rings-prev.png)
