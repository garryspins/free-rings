class Game {
    static VERSION = "1.4";
    static FRAMERATE = 30;
    static TDELTA_TARGET = Math.floor(1000 / Game.FRAMERATE);
    static TEXTAREA = document.querySelector(".textarea.info");
    static RANDOM_BAGS = [
        [1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3, 0, 1, 2, 3],
        [0, 1, 2, 3, 2, 3],
        [0, 1, 2, 3, 0, 1, 2, 3, 2, 3],
    ];
    static tdelta;
    static looprunning;
    static paused;
    static time_elapsed;
    static score;
    static stacks_cleared;
    static current_round;
    static laststamp;
    static tickno;
    static random_bag;

    static init() {
        Game.tdelta = Game.TDELTA_TARGET;
        Game.looprunning = 0;
        Game.paused = 0;
        Game.time_elapsed = 0;
        Game.score = 0;
        Game.stacks_cleared = 0;
        Game.current_round = 1;
        Game.laststamp = null;
        Game.tickno = 0;
        Game.random_bag = [];
    }

    static restart() {
        if (!Game.looprunning)
            Main.init_all();
    }

    static random_stack() {
        return Math.floor(Math.random() * (Stack.NSTACKS - 1));
    }

    /* The new RNG is experimental and uses a tetris-like random bag
     * without the legality test. 8 rings shuffled means a worst case of
     * 4 of the same ring in a row ([0, 1, ... 3, 3] ->
     * [3, 3, 1, 2, 1, ...])
     *
     * After playtesting it, it feels so much fairer.
     */
    static random_ring() {
        if (Game.random_bag.length === 0) {
            let selbag = Game.RANDOM_BAGS[Math.floor(Game.current_round / 2)];

            if (typeof selbag === "undefined")
                selbag = Game.RANDOM_BAGS[4];

            Game.random_bag = [...selbag].sort(() => Math.random() * 3 - 1);
        }

        return Game.random_bag.pop();
    }

    static zeropad(value, width) {
        let str = value.toString();

        while (str.length < width)
            str = "0" + str;

        return str;
    }

    static elapsed_to_string() {
        const SECOND = 1000;
        const MINUTE = SECOND * 60;
        const HOUR = MINUTE * 60;
        let elapsed = Math.floor(Game.time_elapsed);
        let msec;
        let sec;
        let minutes;
        let hours;
        let str = "";

        hours = Math.floor(elapsed / HOUR);
        minutes = Math.floor((elapsed % HOUR) / MINUTE);
        sec = Math.floor((elapsed % MINUTE) / SECOND);
        msec = elapsed % SECOND;

        if (hours > 0)
            str += `${hours.toString()}:`;

        str += `${Game.zeropad(minutes, 2)}:${Game.zeropad(sec, 2)}.${Game.zeropad(msec, 3)}`;
        return str;
    }

    static get_stats_info() {
        return `` +
            `Free Rings v${Game.VERSION}\n` +
            `Time:   ${Game.elapsed_to_string()}\n` +
            `Round:  ${Game.current_round}\n` +
            `Clears: ${Game.stacks_cleared}\n` +
            `Score:  ${Game.score}\n` +
            `FPS:    ${Math.floor(1000 / Game.tdelta)}\n` +
            `Frame:  ${Game.tickno}\n` +
            `` ;
    }

    static tick(curstamp) {
        let diff;

        if (Game.laststamp === null) {
            Game.laststamp = curstamp;
            return;
        }

        diff = curstamp - Game.laststamp;
        Game.laststamp = curstamp;
        Game.tdelta = diff;
        Game.time_elapsed += Game.tdelta;
        ++Game.tickno;
    }

    /* 1. Fixes a rendering bug where the last frame of the ring_move
     *    animation is visible on game over if the timer ran out. Also
     *    added this to the pause screen cause I thought it looked nice.
     */
    static loop(timestamp) {
        let score;

        Draw.clear();

        if (Game.paused) {
            Draw.draw_stacks(); // 1
            Draw.draw_pause_screen();
            Game.laststamp = timestamp;
            requestAnimationFrame(Game.loop);
            return;
        }

        Game.tick(timestamp);
        score = Stack.check_ringsets();

        if (score > 0) {
            if (++Game.stacks_cleared % 10 == 0) {
                ++Game.current_round;
                Stack.reset();
                Animation.push(new Animation({
                    type:      "new_round",
                    starttime: timestamp,
                    roundno:   Game.current_round,
                    callback:  null,
                }));
            }
        }

        Game.score += score;
        Animation.step_all(timestamp);
        Draw.draw_stacks();
        Draw.draw_random_bag();
        Draw.draw_mouse_ring();
        Game.TEXTAREA.value = Game.get_stats_info();

        if (Stack.check_gameover()) {
            Game.kill();
            Draw.clear();
            Draw.draw_stacks(); // 1
            Draw.draw_game_over();
        }

        if (Game.looprunning)
            requestAnimationFrame(Game.loop);
    }

    static pause() {
        Game.paused = 1;
    }

    /* Putting the held ring back is an anti-cheese measure. You can
     * pause buffer to buy time on account of the animations restarting
     * (which is done to prevent them from breaking), but with this patch
     * all this will do for you is allow you more time to think
     */
    static unpause() {
        Game.paused = 0;
        Animation.restart_animations();
        Mouse.put_held_ring_back();
        Mouse.lb = 0;
    }

    static kill() {
        Game.looprunning = 0;
    }

    static startloop() {
        Game.looprunning = 1;
        Animation.push(new Animation({
            type:      "ring_init",
            starttime: 0,
            callback:  null,
        }));
        requestAnimationFrame(Game.loop);
    }
}
