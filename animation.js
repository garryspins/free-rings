/*
 * generic:
 * [mandatory]
 *     type:      string (see Animation.TYPES)
 *     starttime: timestamp
 *     callback:  fn
 * [calculated]
 *     lasttime:  timestamp
 *     animid:    number
 *     stepfn:    fn (see Animation.STEP_FNS)
 *     zindex:    number (hard coded based on type)
 *
 * ring_move:
 *     src:       stackid
 *     dest:      stackid
 *     ring:      ringtype
 *
 * ring_timer:
 *     selstack: stackid
 *     selring:  ringtype
 *     timeout:  ms
 *
 * ring_init:
 *     none
 *
 * new_round:
 *     roundno: number
 */
class Animation {
    static TYPES = {
        "ring_move":  0,
        "ring_timer": 1,
        "ring_init":  2,
        "new_round":  3,
    };
    static ZINDEX = {
        "ring_move":  2,
        "ring_timer": 1,
        "ring_init":  0,
        "new_round":  3,
    };
    static STEP_FNS = {
        "ring_move":  Animation.step_ring_move,
        "ring_timer": Animation.step_ring_timer,
        "ring_init":  Animation.step_ring_init,
        "new_round":  Animation.step_new_round,
    };
    static CONSTRUCTOR_FNS = {
        "ring_move":  Animation.constructor_ring_move,
        "ring_timer": Animation.constructor_ring_timer,
        "ring_init":  Animation.constructor_ring_init,
        "new_round":  Animation.constructor_new_round,
    };
    static RING_TIMER_LOW_CAP = 1000;
    static RING_TIMER_DECAY = 30;
    static list;
    static animid;
    static ring_timer_timeout;

    static init() {
        Animation.list = [];
        Animation.animid = 0;
        Animation.ring_timer_timeout = 10000;
    }

    constructor(obj) {
        if (!(obj.type in Animation.TYPES)) {
            console.error(`invalid animation type '${obj.type}'`);
            return;
        }

        this.type = obj.type;
        this.starttime = obj.starttime;
        this.callback  = obj.callback;

        this.lasttime = obj.starttime;
        Animation.animid += 1;
        this.animid = Animation.animid;
        this.stepfn = Animation.STEP_FNS[obj.type];
        this.zindex = Animation.ZINDEX[obj.type];
        Animation.CONSTRUCTOR_FNS[obj.type](this, obj);
    }

    static constructor_ring_move(selfref, obj) {
        selfref.src       = obj.src;
        selfref.dest      = obj.dest;
        selfref.ring      = obj.ring;
    }

    static constructor_ring_timer(selfref, obj) {
        selfref.selstack = obj.selstack;
        selfref.selring = obj.selring;
        selfref.timeout = obj.timeout;
    }

    static constructor_ring_init(selfref, obj) {
        return;
    }

    static constructor_new_round(selfref, obj) {
        selfref.roundno = obj.roundno;
    }

    /* Linear Interpolation: A + (t * (B - A) / resolution)
     */
    static step_ring_move(selanim, curstamp) {
        const resolution = 100;
        let diff = curstamp - selanim.starttime;
        let step = diff;
        let cs = Stack.get_coords(selanim.src);
        let ce = Stack.get_coords(selanim.dest);
        let outx = cs[0] + (step * (ce[0] - cs[0]) / resolution);
        let outy = cs[1] + (step * (ce[1] - cs[1]) / resolution);

        Draw.draw_ring(outx, outy, selanim.ring);
        selanim.lasttime = curstamp;
        return step < resolution;
    }

    static step_ring_timer(selanim, curstamp) {
        let diff;
        let text;
        let centerxy;

        if (selanim.interrupt)
            return false;

        diff = curstamp - selanim.starttime;
        text = Math.floor((selanim.timeout - diff) / 1000);
        centerxy = Stack.get_coords(6);
        Draw.line_to_stack(6, selanim.selstack, 0);
        Draw.draw_ring(centerxy[0], centerxy[1], selanim.selring);
        Draw.center_text(text + 1);
        selanim.lasttime = curstamp;
        return diff < selanim.timeout;
    }

    static step_ring_init(selanim, curstamp) {
        let selstack = Game.random_stack();
        let selring = Game.random_ring();

        Stack.push(6, selring);

        Animation.push(new Animation({
            type:      "ring_timer",
            starttime: curstamp,
            selstack:  selstack,
            selring:   selring,
            timeout:   Animation.ring_timer_timeout,
            callback:  Animation.callback_ring_timer,
        }));

        return false;
    }

    static step_new_round(selanim, curstamp) {
        let diff = curstamp - selanim.starttime;
        selanim.lasttime = curstamp;
        Draw.draw_round_text(selanim.roundno);
        return diff < 2000;
    }

    static callback_ring_move(selanim) {
        Stack.push(selanim.dest, selanim.ring);

        Animation.push(new Animation({
            type:      "ring_init",
            starttime: selanim.lasttime,
            callback:  null,
        }));
    }

    static callback_ring_timer(selanim) {
        let stackdest = selanim.selstack;
        let centerring = selanim.selring;

        if (Animation.ring_timer_timeout > Animation.RING_TIMER_LOW_CAP)
            Animation.ring_timer_timeout -= Animation.RING_TIMER_DECAY;

        Animation.push(new Animation({
            type:      "ring_move",
            starttime: selanim.lasttime,
            src:       6,
            dest:      stackdest,
            ring:      centerring,
            callback:  Animation.callback_ring_move,
        }));
    }

    static push(anim) {
        Animation.list.push(anim);
        Animation.list.sort((a, b) => a.zindex - b.zindex);
        return Animation.animid;
    }

    static cancel(animid) {
        let target = Animation.list.find(anim => anim.animid === animid);

        if (typeof target === "undefined")
            return false;

        Animation.list = Animation.list.filter(anim => anim.animid !== animid);
        return true;
    }

    static step_all(curstamp) {
        Animation.list.forEach(function(anim){
            if (anim.stepfn(anim, curstamp) === false) {
                if (Animation.cancel(anim.animid) === false)
                    console.error("Error removing animation ID#${selanim.animid} from queue");

                if (typeof anim.callback === "function")
                    anim.callback(anim);
            }
        });
    }

    static restart_animations() {
        Animation.list.forEach(function(anim){
            anim.starttime = Game.laststamp;
        });
    }
}

