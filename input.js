class Mouse {
    static x;
    static y;
    static lb;
    static heldring;
    static heldringstack;
    static curstack;

    static init() {
        Mouse.x = 0;
        Mouse.y = 0;
        Mouse.lb = 0;
        Mouse.heldring = -1;
        Mouse.heldringstack = -1;
        Mouse.curstack = -1;
        Input.addev("mousemove",  Mouse.ev_mousemove);
        Input.addev("mousedown",  Mouse.ev_mousedown);
        Input.addev("mouseup",    Mouse.ev_mouseup);
        Input.addev("mouseleave", Mouse.ev_mouseleave);
    }

    static get_coords() {
        return [Mouse.x, Mouse.y];
    }

    static set_coords(ev) {
        const x = ev.pageX - ev.currentTarget.offsetLeft;
        const y = ev.pageY - ev.currentTarget.offsetTop;

        Mouse.x = x;
        Mouse.y = y;
    }

    static lmb_down() {
        return Mouse.lb == 1;
    }

    static put_held_ring_back() {
        if (typeof Mouse.heldring === "undefined" || Mouse.heldring === -1)
            return;

        Stack.push(Mouse.heldringstack, Mouse.heldring);
        Mouse.heldring = -1;
        Mouse.heldringstack = -1;
    }

    /* This function prevents you from dragging a ring outside of the
     * canvas. This may cause a couple game overs, but if the ring is
     * allowed to be dragged out of the canvas, then it creates a bug
     * whereing the player can cheat by "deleting" rings that are
     * inconvenient for them. Drag ring out, let go of mouse, mouseup
     * event never fires, then bring the ring to a stack and put it down,
     * it vanishes out of existence. This happens because when the
     * mouseup event is unable to fire, the game thinks you are in a
     * mouse down state, but since you are unable to fire a mouseup event
     * as the mouse button is not down, the only thing to do is trigger
     * another mousedown event, which will grab whatever is at the top of
     * the stack the mouse is over, so the ring is lost. As I have no
     * control over this, the next best thing is to just put the ring
     * back and cancel the mouse down state.
     *
     * For now, this function stays.
     */
    static ev_mouseleave(ev) {
        Mouse.lb = 0;
        Mouse.put_held_ring_back();
    }

    static ev_mousedown(ev) {
        let stackid;

        if (ev.button !== 0)
            return;

        Mouse.set_coords(ev);
        Mouse.lb = 1;
        stackid = Stack.which_stack(Mouse.x, Mouse.y);

        if (stackid === -1)
            return;

        if (stackid === 6){
            Stack.send_ring();
            return;
        }

        Mouse.heldringstack = stackid;
        Mouse.heldring = Stack.pop(stackid);
    }

    static ev_mouseup(ev) {
        let stackid;
        let stackcp;

        Mouse.set_coords(ev);
        Mouse.lb = 0;
        stackid = Stack.which_stack(Mouse.x, Mouse.y);

        if (stackid === -1 || stackid === 6) {
            Mouse.put_held_ring_back();
            return;
        }

        stackcp = Stack.get_stack(stackid);

        if (stackcp.length > 0 && stackcp.pop() >= Mouse.heldring) {
            Mouse.put_held_ring_back();
            return;
        }

        if (typeof Mouse.heldring !== "undefined" && Mouse.heldring !== -1)
            Stack.push(stackid, Mouse.heldring);

        Mouse.heldring = -1;
        Mouse.heldringstack = -1;
    }

    static ev_mousemove(ev) {
        Mouse.set_coords(ev);
        Mouse.curstack = Stack.which_stack(Mouse.x, Mouse.y);
    }
}

class Keyboard {
    static keysdown;
    static map;

    static init() {
        Keyboard.keysdown = {};
        Keyboard.map = [
            [" ", Stack.send_ring],
            ["r", Game.restart],
            ["p", Keyboard.toggle_pause]
        ];

        Input.addev("keydown", Keyboard.ev_keydown);
        Input.addev("keyup",   Keyboard.ev_keyup);

        Keyboard.map.forEach(mapping => {
            Keyboard.keysdown[mapping[0]] = false;
        });
    }

    static toggle_pause() {
        if (Game.paused)
            Game.unpause();
        else
            Game.pause();
    }

    static ev_keyup(ev) {
        Keyboard.keysdown[ev.key] = false;
    }

    static ev_keydown(ev) {
        ev.preventDefault();
        Keyboard.keysdown[ev.key] = true;

        Keyboard.map.forEach(mapping => {
            if (Keyboard.keysdown[mapping[0]])
                mapping[1]();
        });
    }
}

class Input {
    static init() {
        Input.addev("blur",  Input.ev_blur);
        Input.addev("focus", Input.ev_focus);
        Mouse.init();
        Keyboard.init();
    }

    /* 1. avoid duplicate event listeners. Removing events that aren't
     *    registered seems to work just fine
     */
    static addev(eventname, eventfn) {
        Draw.CANVAS.removeEventListener(eventname, eventfn); // 1
        Draw.CANVAS.addEventListener(eventname, eventfn);
    }

    static ev_blur(ev) {
        Game.pause();
        Mouse.lb = 0;
        console.log("Game lost focus");
    }

    static ev_focus(ev) {
        Game.unpause();
    }
}
