class Main {
    static main(){
        console.log(`Free Rings v${Game.VERSION}`);
        Main.init_all();
        Draw.CANVAS.focus();
    }

    static init_all() {
        Stack.init();
        Input.init();
        Draw.init();
        Animation.init();
        Game.init();
        Game.startloop();
    }
}
