/* 
    Solarized palette courtesy Ethan Schoonover https://ethanschoonover.com/solarized/
*/
class Draw {
    static CANVAS = document.querySelector(".canvas.game");
    static CTX = Draw.CANVAS.getContext("2d");
    static CANVAS_RB = document.querySelector(".canvas.randbag");
    static CTX_RB = Draw.CANVAS_RB.getContext("2d");
    static WIDTH = 500;
    static HEIGHT = 500;
    static RB_WIDTH = 500;
    static RB_HEIGHT = 50;
    static THEME = "dark";
    static RINGTHEME = "tritanopia";

    static CSS_BGCOLOR_THEMES = {
        "light":     "#fff",
        "dark":      "#161616",
        "solarized": "#002b36",
    };
    static BGCOLOR_THEMES = {
        "light":     "#f7f7ff",
        "dark":      "#161616",
        "solarized": "#002b36",
    };
    static BGCOLOR_TRANS_THEMES = {
        "light":     "rgba(247, 247, 255, .8)",
        "dark":      "rgba(22, 22, 22, .8)",
        "solarized": "rgba(7, 54, 66, .8)",
    };
    static RING_COLORS_THEMES = {
        "light":     [ "#3e3",    "#f33",    "#33f",    "#f70"     ],
        "dark":      [ "#3e3",    "#f33",    "#0ef",    "#ff0"     ],
        "solarized": [ "#6c71c4", "#cb4b16", "#268bd2", "#eee8d5", ],

        // Colorblind Themes
        "protanopia": [ "#e8ce2f", "#8b7f54", "#a3a3b3", "#2c5bb0"],
        "deuteranopia": [ "#e8ce2f", "#8b7f54", "#a3a3b3", "#2c5bb0"],
        "tritanopia": [ "#75ddee", "#ec4144", "#00656c", "#e5aebc" ]
    };

    static RING_PLACEHOLDER_THEMES = {
        "light":     "#aaa",
        "dark":      "#333",
        "solarized": "#586e75",
    };
    static TEXT_COLOR_FG_THEMES = {
        "light":     "#000",
        "dark":      "#fff",
        "solarized": "#fdf6e3",
    };
    static TEXT_COLOR_BG_THEMES = {
        "light":     "rgba(255, 255, 255, .9)",
        "dark":      "rgba(22, 22, 22, .9)",
        "solarized": "rgba(0, 43, 54, .9)",
    };

    static LINE_COLOR_THEMES = {
        "light":     ["#f00", "#000"],
        "dark":      ["#444", "#fff"],
        "solarized": ["#b58900", "#fdf6e3"],
    };
    static BGCOLOR;
    static BGCOLOR_TRANS;
    static RING_COLORS;
    static TEXT_COLOR_FG;
    static TEXT_COLOR_BG;
    static LINE_COLOR;

    /* 1. From testing, this doesn't seem to be necessary, but it's kept
     *    just in case of browser incompatibilities.
     *    tabIndex makes a DOM element "focusable", so that it can
     *    receive events, like onkeyup
     */
    static init() {
        let textinfo = document.querySelector(".info");

        Draw.CANVAS.width = Draw.WIDTH;
        Draw.CANVAS.height = Draw.HEIGHT;
        Draw.CANVAS_RB.width = Draw.RB_WIDTH;
        Draw.CANVAS_RB.height = Draw.RB_HEIGHT;
        Draw.clear();
        Draw.CANVAS.tabIndex = 0; // 1

        document.body.style.background = Draw.CSS_BGCOLOR_THEMES[Draw.THEME];
        textinfo.style.background = Draw.CSS_BGCOLOR_THEMES[Draw.THEME];
        textinfo.style.color = Draw.TEXT_COLOR_FG_THEMES[Draw.THEME];
        Draw.BGCOLOR = Draw.BGCOLOR_THEMES[Draw.THEME];
        Draw.BGCOLOR_TRANS = Draw.BGCOLOR_TRANS_THEMES[Draw.THEME];
        Draw.RING_COLORS = Draw.RING_COLORS_THEMES[Draw.RINGTHEME];
        Draw.TEXT_COLOR_FG = Draw.TEXT_COLOR_FG_THEMES[Draw.THEME];
        Draw.TEXT_COLOR_BG = Draw.TEXT_COLOR_BG_THEMES[Draw.THEME];
        Draw.LINE_COLOR = Draw.LINE_COLOR_THEMES[Draw.THEME];
        Draw.PLACEHOLDER_RING = Draw.RING_PLACEHOLDER_THEMES[Draw.THEME];
    }

    static clear() {
        let oldstyle = Draw.CTX.fillStyle;

        Draw.CTX.fillStyle = Draw.BGCOLOR;
        Draw.CTX.fillRect(0, 0, Draw.WIDTH, Draw.HEIGHT);
        Draw.CTX.fillStyle = oldstyle;
        Draw.CTX_RB.fillStyle = Draw.BGCOLOR;
        Draw.CTX_RB.fillRect(0, 0, Draw.RB_WIDTH, Draw.RB_HEIGHT);
    }

    static draw_transparent_background() {
        let oldstyle = Draw.CTX.fillStyle;

        Draw.CTX.fillStyle = Draw.BGCOLOR_TRANS;
        Draw.CTX.fillRect(0, 0, Draw.WIDTH, Draw.HEIGHT);
        Draw.CTX.fillStyle = oldstyle;
    }

    static path_circle(x, y, size, fn) {
        let res;

        Draw.CTX.beginPath();
        Draw.CTX.arc(x, y, size, 0, Math.PI * 2);

        if (typeof fn === "function")
            res = fn(Draw.CTX);

        Draw.CTX.closePath();
        return res;
    }

    static draw_ring(x, y, ringvalue) {
        Draw.path_circle(x, y, Stack.RING_INNER_SIZE + Stack.RING_SIZES[ringvalue]);
        if (ringvalue == 4) {
            Draw.CTX.fillStyle = Draw.PLACEHOLDER_RING;
        } else {
            Draw.CTX.fillStyle = Draw.RING_COLORS[ringvalue];
        }

        Draw.CTX.fill();

        Draw.path_circle(x, y, Stack.RING_INNER_SIZE);
        Draw.CTX.fillStyle = Draw.BGCOLOR;
        Draw.CTX.fill();
    }

    static draw_tiny_ring(x, y, ringvalue) {
        Draw.CTX_RB.beginPath();
        Draw.CTX_RB.arc(x, y, 5 + Stack.RING_SIZES[6], 0, Math.PI * 2);
        Draw.CTX_RB.fillStyle = Draw.RING_COLORS[ringvalue];
        Draw.CTX_RB.fill();
        Draw.CTX_RB.closePath();

        Draw.CTX_RB.beginPath();
        Draw.CTX_RB.arc(x, y, 5, 0, Math.PI * 2);
        Draw.CTX_RB.fillStyle = Draw.BGCOLOR;
        Draw.CTX_RB.fill();
        Draw.CTX_RB.closePath();
    }

    static draw_random_bag() {
        Game.random_bag.forEach(function(ring, i) {
            Draw.draw_tiny_ring((i + 1) * 45, Draw.RB_HEIGHT / 2, ring);
        });
    }

    static line_to_stack(stack1, stack2, coloridx) {
        let stack1xy = Stack.COORDS[stack1];
        let stack2xy = Stack.COORDS[stack2];
        let color = Draw.LINE_COLOR[coloridx];

        if (typeof stack1xy === "undefined" || typeof stack2xy === "undefined")
            return false;

        Draw.CTX.strokeStyle = color;
        Draw.CTX.lineWidth = 5;
        Draw.CTX.lineCap = "round";
        Draw.CTX.beginPath();
        Draw.CTX.moveTo(stack1xy[0], stack1xy[1]);
        Draw.CTX.lineTo(stack2xy[0], stack2xy[1]);
        Draw.CTX.stroke();
        Draw.CTX.closePath();

        return true;
    }

    static center_text(text) {
        let str = text.toString();

        Draw.CTX.fillStyle = Draw.TEXT_COLOR_FG;
        Draw.CTX.font = "bold 30px monospace";
        Draw.CTX.textAlign = "center";
        Draw.CTX.textBaseline = "middle";
        Draw.CTX.fillText(text, Draw.WIDTH / 2, Draw.HEIGHT / 2);
    }

    static draw_round_text(round) {
        Draw.CTX.fillStyle = Draw.TEXT_COLOR_BG;
        Draw.CTX.fillRect(0, Draw.HEIGHT / 2 - 30, Draw.WIDTH, 60);
        Draw.CTX.fillStyle = Draw.TEXT_COLOR_FG;
        Draw.CTX.font = "bold 50px monospace";
        Draw.CTX.textAlign = "center";
        Draw.CTX.textBaseline = "middle";
        Draw.CTX.fillText(`Round ${round}`, Draw.WIDTH / 2, Draw.HEIGHT / 2);
    }

    static draw_pause_screen() {
        Draw.draw_transparent_background();
        Draw.CTX.fillStyle = Draw.TEXT_COLOR_FG;
        Draw.CTX.font = "bold 100px monospace";
        Draw.CTX.textAlign = "center";
        Draw.CTX.textBaseline = "middle";
        Draw.CTX.fillText("Paused", Draw.WIDTH / 2, Draw.HEIGHT / 2);
    }

    static draw_game_over() {
        Draw.draw_transparent_background();
        Draw.CTX.fillStyle = Draw.TEXT_COLOR_FG;
        Draw.CTX.font = "bold 80px monospace";
        Draw.CTX.textAlign = "center";
        Draw.CTX.textBaseline = "middle";
        Draw.CTX.fillText("GAME", Draw.WIDTH / 2, Draw.HEIGHT / 2 - 30);
        Draw.CTX.fillText("OVER", Draw.WIDTH / 2, Draw.HEIGHT / 2 + 30);
        Draw.CTX.font = "bold 30px monospace";
        Draw.CTX.fillText("Press R to restart", Draw.WIDTH / 2, Draw.HEIGHT / 2 + 90);
    }

    static draw_stacks() {
        for (let i = 0; i < Stack.NSTACKS - 1; ++i) {
            const stack_pos = Stack.COORDS[i];
            let selstack = Stack.get_stack(i);

            Draw.draw_ring(stack_pos[0], stack_pos[1], 4);

            selstack.forEach(function(ring){
                Draw.draw_ring(stack_pos[0], stack_pos[1], ring);
            });
        }
    }

    static draw_mouse_ring() {
        if (typeof Mouse.heldring === "undefined" || Mouse.heldring === -1)
            return;

        Draw.draw_ring(Mouse.x, Mouse.y, Mouse.heldring);
        Draw.line_to_stack(Mouse.heldringstack, Mouse.curstack, 1);
    }
}
