class Stack {
    // 0, 1, 2, 3, grey, hitbox, tiny (random bag preview)
    static RING_SIZES = [50, 40, 30, 15, 50, 90, 15];
    static RING_INNER_SIZE = 20;
    static NSTACKS = 7;
    static COORDS = [
        [170, 70],
        [330, 70],
        [70,  250],
        [430, 250],
        [170, 430],
        [330, 430],
        [250, 250],
    ];
    static STACKS_TEMPLATE = [
        [],
        [],
        [],
        [],
        [],
        [],
        [],
    ];
    static stacks;

    static init() {
        Stack.stacks = Stack.STACKS_TEMPLATE.map(arr => []);
    }

    /* I honestly have no idea why this function is necessary, but it
     * fixes a bug wherein the timer animation throws away the ring and
     * it disappears into the void immediately following a new round.
     * I suspected it had to do with a garbage collected reference (which
     * is weird cause I only remember storing numbers), and when I
     * implemented this function to init but preserve stacks[6], the bug
     * was fixed. My intuition was spot on as, as it turns out, in
     * Animation::callback_ring_timer, there was a call to Stack.pop()
     * instead of selanim.selring. I have since fixed this and this
     * function may not be necessary. Further testing needed
     */

    static reset() {
        let stack6 = Stack.stacks[6];

        Stack.init();
        Stack.stacks[6] = stack6;
    }

    /* The detection range is now much larger than the grey circles, so
     * it feels a lot better to play now
     */
    static which_stack(x, y) {
        for (let i = 0; i < Stack.COORDS.length; ++i)
            if (Draw.path_circle(Stack.COORDS[i][0], Stack.COORDS[i][1], Stack.RING_SIZES[5], ctx => ctx.isPointInPath(x, y)))
                return i;

        return -1;
    }

    static send_ring() {
        let selanim;
        let ring;
        let stacktarget;
        let stacktargettop;

        selanim = Animation.list.find(anim => anim.type === "ring_timer");

        if (typeof selanim === "undefined")
            return false;

        ring = Stack.get_stack(6).pop();
        stacktarget = Stack.get_stack(selanim.selstack);
        stacktargettop = [...stacktarget].pop();

        if (stacktarget.length === 0 || stacktargettop < ring)
            selanim.interrupt = 1;

        return true;
    }

    static check_gameover() {
        let results = Stack.stacks.slice(0, Stack.NSTACKS - 1).filter(stack => {
            for (let i = 0; i < stack.length - 1; ++i)
                if (stack[i] >= stack[i + 1])
                    return true;

            return false;
        });

        return results.length > 0;
    }

    static check_ringsets() {
        let total = 0;

        for (let i = 0; i < Stack.NSTACKS - 1; ++i) {
            let selstackcp = Stack.get_stack(i);

            if (selstackcp.pop() !== 3)
                continue;

            if (selstackcp.pop() !== 2)
                continue;

            total += 50;
            Stack.pop(i);
            Stack.pop(i);

            if (selstackcp.pop() !== 1)
                continue;

            total += 50;
            Stack.pop(i);

            if (selstackcp.pop() !== 0)
                continue;

            total += 150;
            Stack.pop(i);
        }

        return total;
    }

    static center_empty() {
        let selstack = Stack.get_stack(6);

        return selstack.length === 0;
    }

    static push(stackid, ring) {
        let selstack = Stack.stacks[stackid];

        if (stackid === 6 && selstack.length > 0)
            selstack.pop();

        selstack.push(ring);

        if (typeof ring !== "number")
            throw "Stack.push received non-number. Please investigate.";
    }

    static pop(stackid) {
        let selstack = Stack.stacks[stackid];

        return selstack.pop();
    }

    static get_coords(stackid) {
        return [...Stack.COORDS[stackid]];
    }

    static get_stack(stackid) {
        let selstack = Stack.stacks[stackid];

        return [...selstack];
    }
}
